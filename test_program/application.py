import datetime
import os
import wave

import pyaudio
import wx

import decoder
import recorder


class Example(wx.Frame):
    def __init__(self, parent, title):
        super(Example, self).__init__(parent, title=title, size=(450, 250))

        self.rec = recorder.Recorder(rate=16000)
        self.file_counter = 0
        self.recording = False

        self.dec = decoder.Decoder()
        self.decoding = False

        self.last_analyze_time = -1
        self.start_time = None
        self.end_time = None
        self.last_audio_time = -1

        self.curr = None
        self.desired_text = None
        self.result_phones = None

        self.init_ui()
        self.Centre()
        self.Show()

    def init_ui(self):

        panel = wx.Panel(self)

        vertical_box = wx.BoxSizer(wx.VERTICAL)

        desired_text_box = wx.BoxSizer(wx.HORIZONTAL)
        static_text = wx.StaticText(panel, label='Desired Text')
        desired_text_box.Add(static_text, flag=wx.RIGHT, border=8)
        self.desired_text = wx.TextCtrl(panel)
        desired_text_box.Add(self.desired_text, proportion=1)
        vertical_box.Add(desired_text_box, flag=wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, border=10)

        vertical_box.Add((-1, 25))

        phone_horizontal_box = wx.BoxSizer(wx.HORIZONTAL)
        st1 = wx.StaticText(panel, label='Phones')
        phone_horizontal_box.Add(st1, flag=wx.RIGHT, border=8)
        self.result_phones = wx.TextCtrl(panel)
        phone_horizontal_box.Add(self.result_phones, proportion=1)
        vertical_box.Add(phone_horizontal_box, flag=wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, border=10)

        vertical_box.Add((-1, 25))

        buttons_horizontal_box = wx.BoxSizer(wx.HORIZONTAL)
        record_button = wx.Button(panel, label='Record')
        record_button.Bind(wx.EVT_BUTTON, self.start)
        buttons_horizontal_box.Add(record_button, flag=wx.EXPAND)
        stop_button = wx.Button(panel, label='Stop')
        stop_button.Bind(wx.EVT_BUTTON, self.stop)
        buttons_horizontal_box.Add(stop_button, flag=wx.EXPAND)
        analyze_button = wx.Button(panel, label='Analyze')
        analyze_button.Bind(wx.EVT_BUTTON, self.analyze)
        buttons_horizontal_box.Add(analyze_button, flag=wx.EXPAND)
        replay_button = wx.Button(panel, label='Replay')
        replay_button.Bind(wx.EVT_BUTTON, self.replay)
        buttons_horizontal_box.Add(replay_button, flag=wx.EXPAND)
        clear_button = wx.Button(panel, label='Clear')
        clear_button.Bind(wx.EVT_BUTTON, self.clear)
        buttons_horizontal_box.Add(clear_button, flag=wx.EXPAND)
        vertical_box.Add(buttons_horizontal_box, flag=wx.CENTER, border=10)

        panel.SetSizer(vertical_box)

    def start(self, e):
        if not self.recording:
            base_path = 'audio/'
            if not os.path.exists('audio'):
                os.makedirs('audio')
            desired_text = self.desired_text.GetValue()
            new_path = base_path + desired_text.replace(' ', '_')
            new_path = new_path.lower()
            if self.desired_text.GetValue() != '':
                if not os.path.exists(new_path):
                    os.makedirs(new_path)
                base_path = new_path + '/'
            print 'Start Recording:', base_path + str(self.file_counter) + '.wav'
            self.curr = self.rec.open(base_path + str(self.file_counter) + '.wav')
            self.recording = True
            self.start_time = datetime.datetime.utcnow()
            self.curr.start_recording()

    def stop(self, e):
        if self.recording:
            print 'Stop Recording:', self.curr.file_name
            try:
                self.curr.stop_recording()
                self.end_time = datetime.datetime.utcnow()
                self.last_audio_time = divmod((self.end_time - self.start_time).total_seconds(), 60)
                self.file_counter += 1
                self.recording = False
            except:
                print 'Failure to stop recording'

    def analyze(self, e):
        try:
            print 'Analyzing Last Recording:', self.curr.file_name
        except:
            print ''
            return

        analyze_start_time = datetime.datetime.utcnow()
        self.dec.analyze(self.curr.file_name)
        analyze_stop_time = datetime.datetime.utcnow()
        self.last_analyze_time = divmod((analyze_stop_time - analyze_start_time).total_seconds(), 60)
        self.result_phones.SetValue(str(self.dec.get_results()))

        # Save all data to the same file if the desired text does not change
        desired_text = self.desired_text.GetValue()
        if desired_text != '':
            desired_phones = ['SIL']
            for word in desired_text.split(' '):
                phones = self.dec.lookup_word(word)
                if phones is not None:
                    desired_phones.extend(phones)
                    desired_phones.append('SIL')
            data_insert = str(self.file_counter - 1) + ';' + desired_text + ';' + str(
                self.dec.get_results()) + ';' + str(self.last_audio_time) + ';' + str(self.last_analyze_time) + '\n'
            target_file = 'audio/' + desired_text.replace(' ', '_') + '/data.txt'
            target_file = target_file.lower()
            try:
                if os.path.isfile(target_file):
                    f = open(target_file, 'a')
                    f.write(data_insert)
                    f.close()
                else:
                    f = open(target_file, 'w')
                    f.write('Target Text;' + desired_text + ';' + str(desired_phones) + ';Audio Length;Analysis Time\n')
                    f.write(data_insert)
                    f.close()
            except IOError:
                print 'Failure to save data; possibly changed desired text'

    def replay(self, e):
        try:
            print 'Replay Last Recording:', self.curr.file_name
        except:
            print ''
            return None
        # length of data to read.
        chunk = 1024

        '''
        ************************************************************************
              This is the start of the "minimum needed to read a wave"
        ************************************************************************
        '''
        # open the file for reading.
        try:
            wf = wave.open(self.curr.file_name, 'rb')
        except:
            print 'Failure to open wav file'
            return None

        # create an audio object
        p = pyaudio.PyAudio()

        # open stream based on the wave object which has been input.
        stream = p.open(format=p.get_format_from_width(wf.getsampwidth()),
                        channels=wf.getnchannels(),
                        rate=wf.getframerate(),
                        output=True)

        # read data (based on the chunk size)
        data = wf.readframes(chunk)

        # play stream (looping from beginning of file to the end)
        while data != '':
            # writing to the stream is what *actually* plays the sound.
            stream.write(data)
            data = wf.readframes(chunk)

        # cleanup stuff.
        stream.close()
        p.terminate()

    def clear(self, e):
        print 'Clear Phones and desired text'
        self.result_phones.Clear()
        self.desired_text.Clear()


if __name__ == '__main__':
    app = wx.App()
    Example(None, title='Simple Test Program')
    app.MainLoop()
