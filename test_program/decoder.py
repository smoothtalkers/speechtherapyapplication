from __future__ import print_function

import os

from pocketsphinx import Pocketsphinx, get_model_path


class Decoder:
    def __init__(self):
        model_path = get_model_path()
        # data_path = get_data_path()

        config = {
            'hmm': os.path.join(model_path, 'en-us'),
            'lm': os.path.join(model_path, 'en-us.lm.bin'),
            'dict': os.path.join(model_path, 'cmudict-en-us.dict'),
            'allphone': os.path.join(model_path, 'en-us-phone.lm.bin')
        }

        self.ps = Pocketsphinx(**config)

    def analyze(self, file_name):
        self.ps.decode(
            audio_file=file_name,
            buffer_size=2048,
            no_search=False,
            full_utt=False
        )

    def get_results(self):
        return self.ps.segments()

    def lookup_word(self, word):
        try:
            return self.ps.lookup_word(str(word).lower()).split(' ')
        except AttributeError:
            return None
