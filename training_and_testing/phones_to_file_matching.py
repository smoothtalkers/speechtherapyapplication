import re

from pocketsphinx import Pocketsphinx, get_model_path

ps = Pocketsphinx()

file_to_phrase = {}
print get_model_path()


def lookup_word(word):
    try:
        return ps.lookup_word(str(word).lower()).lower()
    except AttributeError:
        return ""


def generate_phrase_for_file(phrase):
    result = "<s> "
    for word in phrase.split(' '):
        result += lookup_word(word) + " "
    result += "</s>"
    return result


def generate_phone_dict():
    all_words = set()
    # Generate set from all.map
    for line in open('all.map', 'r'):
        id = line.split(' ')[0].lower()
        phrase = re.findall(r'"([^"]*)"', line)[0].upper()
        for word in phrase.split(' '):
            if '-' in word:
                words = word.split('-')
                all_words.add(words[0])
                all_words.add(words[1])
                continue
            word.strip("'")
            all_words.add(word)
    f = open('cslukids/etc/cslukids.dict', 'w')
    for word in all_words:
        result = lookup_word(word)
        if result == "":
            result = lookup_word(word[0:-1])
            if result == "":
                print 'Word not found: ' + word
                continue
        f.write(word + " " + result.upper() + "\n")

def main():
    # Generate dictionary from all.map
    for line in open('all.map', 'r'):
        id = line.split(' ')[0].lower()
        phrase = re.findall(r'"([^"]*)"', line)[0]
        file_to_phrase[id] = phrase

    c = 1
    fileids = 'cslukids/etc/cslukids_train.fileids'
    transcription = 'cslukids/etc/cslukids_train.transcription'
    f = open(transcription, 'w')
    for line in open(fileids, 'r'):
        if line == "": continue
        filename = line.split('/')[1][0:-1]
        c += 1
        id = filename[5:7]
        try:
            f.write("<s> " + file_to_phrase[id].upper() + " </s>" + " (" + filename + ")\n")
        except KeyError:
            print 'Failure on line ' + str(c) + ' of ' + fileids + "; id = " + id + "; filename = " + filename
    f.close()

    c = 1
    fileids = 'cslukids/etc/cslukids_test.fileids'
    transcription = 'cslukids/etc/cslukids_test.transcription'
    f = open(transcription, 'w')
    for line in open(fileids, 'r'):
        if line == "": continue
        filename = line.split('/')[1][0:-1]
        c += 1
        id = filename[5:7]
        try:
            f.write("<s> " + file_to_phrase[id].upper() + " </s>" + " (" + filename + ")\n")
        except KeyError:
            print 'Failure on line ' + str(c) + ' of ' + fileids + "; id = " + id + "; filename = " + filename
    f.close()

main()
generate_phone_dict()
