import re

import string

from pocketsphinx import Pocketsphinx, get_model_path

ps = Pocketsphinx()

file_to_phrase = {}
print get_model_path()


def lookup_word(word):
    if word == "snowmobiling":
	return "S N OW M OW B IY L IY NG"
    if word == "waterski":
	return "W AA T ER S K IY"
    if word == "tannish":
	return "T AE N IH SH"
    if word == "blondish":
	return "B L AA N D IH SH"
    if word == "unscrewed":
	return "AH N S K R UW D"
    if word == "snowboarding":
	return "S N OW B AO R D IY NG"
    if word == "fresca":
	return "F R EH S K AH"
    if word == "knottsberry":
	return "N AA T S B EH R IY"
    if word == "horsie":
	return "HH AO R S IY"
    if word == "cursive":
	return "K ER S IH V"
    if word == "crawdad":
	return "K R AA D AE D"
    if word == "waterskiing":
	return "W AO T ER S K IY IH NG"
    if word == "thingies":
	return "TH IY NG IY Z"
    if word == "hardboiled":
	return "HH AA R D B OY L D"
    if word == "waverunner":
	return "W EY V R AH N ER"
    if word == "pinata":
	return "P IH N Y AA T AH"
    if word == "alderon":
	return "AA L D ER AA N"
    if word == "vcr":
	return "V IY S IY AA R"
    if word == "seahorse":
	return "S IY HH AO R S"
    if word == "unshrink":
	return "AH N SH R IY N K"
    if word == "josh's":
	return "JH AA SH Z"
    try:
        return ps.lookup_word(str(word).lower()).upper()
    except AttributeError:
	return spelling_to_phones(word)

def spelling_to_phones(word):
    phonelist = ""
    for char in word:
	if char == 'a':
	    phonelist = phonelist + "AA "
	if char == 'b':
	    phonelist = phonelist + "B "
	if char == 'c':
	    phonelist = phonelist + "K "
	if char == 'd':
	    phonelist = phonelist + "D "
	if char == 'e':
	    phonelist = phonelist + "EH "
	if char == 'f':
	    phonelist = phonelist + "F "
	if char == 'g':
	    phonelist = phonelist + "G "
	if char == 'h':
	    phonelist = phonelist + "HH "
	if char == 'i':
	    phonelist = phonelist + "IY "
	if char == 'j':
	    phonelist = phonelist + "JH "
	if char == 'k':
	    phonelist = phonelist + "K "
	if char == 'l':
	    phonelist = phonelist + "L "
	if char == 'm':
	    phonelist = phonelist + "M "
	if char == 'n':
	    phonelist = phonelist + "N "
	if char == 'o':
	    phonelist = phonelist + "OW "
	if char == 'p':
	    phonelist = phonelist + "P "
	if char == 'q':
	    phonelist = phonelist + "K W "
	if char == 'r':
	    phonelist = phonelist + "R "
	if char == 's':
	    phonelist = phonelist + "S "
	if char == 't':
	    phonelist = phonelist + "T "
	if char == 'u':
	    phonelist = phonelist + "UH "
	if char == 'v':
	    phonelist = phonelist + "V "
	if char == 'w':
	    phonelist = phonelist + "W "
	if char == 'x':
	    phonelist = phonelist + "K S "
	if char == 'y':
	    phonelist = phonelist + "Y "
	if char == 'z':
	    phonelist = phonelist + "Z "
    return phonelist

def generate_phrase_for_file(phrase):
    result = "<s> "
    for word in phrase.split(' '):
        result += lookup_word(word) + " "
    result += "</s>"
    return result


def generate_phone_dict():
    all_words = set()
    # Generate set from transcripts

    for line in open('cslukids/etc/cslukids_train.transcription', 'r'):
        words = line.split(' ')
	for word in words:
	    if word == "":
		continue
	    if word[0] != '<' and word[0] != '(':
        	all_words.add(word)

    for line in open('cslukids/etc/cslukids_test.transcription', 'r'):
        words = line.split(' ')
	for word in words:
	    if word == "":
		continue
	    if word[0] != '<' and word[0] != '(':
        	all_words.add(word)

    count = 1

    f = open('cslukids/etc/cslukids.dict', 'w')
    for word in all_words:
        result = lookup_word(word)
        if result == "":
            result = lookup_word(word[0:-1])
            if result == "":
		count += 1
                print str(count) + ' ' + 'Word not found: ' + word
                continue
        f.write(word + " " + result + "\n")

def gettranscript(foldername, filename):
    transcript = 'transcripts/'+foldername+'/'+filename+'.txt'
    f = open(transcript, 'r')
    line = f.readline()
    line = string.replace(line, '<', ' <')
    line = string.replace(line, '>', '> ')
    line = string.replace(line, '\n', '')
    line = re.sub(r"\[|\]|\*", '', line)
    line = line[0:-1]
    line = ' '+line+' '
    line = re.sub(r"  *", ' ', line)
    return line

def main():
    c = 1
    fileids = 'cslukids/etc/cslukids_train.fileids'
    transcription = 'cslukids/etc/cslukids_train.transcription'
    f = open(transcription, 'w')
    for line in open(fileids, 'r'):
        if line == "": continue
        filename = line.split('/')[1][0:-1]
	foldername = line.split('/')[0]
        c += 1
        try:
            f.write("<s>" + gettranscript(foldername, filename) + "</s>" + " (" + filename + ")\n")
        except KeyError:
            print 'Failure on line ' + str(c) + ' of ' + fileids + "; id = " + id + "; filename = " + filename
    f.close()

    c = 1
    fileids = 'cslukids/etc/cslukids_test.fileids'
    transcription = 'cslukids/etc/cslukids_test.transcription'
    f = open(transcription, 'w')
    for line in open(fileids, 'r'):
        if line == "": continue
        filename = line.split('/')[1][0:-1]
	foldername = line.split('/')[0]
        c += 1
        try:
            f.write("<s>" + gettranscript(foldername, filename) + "</s>" + " (" + filename + ")\n")
        except KeyError:
            print 'Failure on line ' + str(c) + ' of ' + fileids + "; id = " + id + "; filename = " + filename
    f.close()

main()
generate_phone_dict()
