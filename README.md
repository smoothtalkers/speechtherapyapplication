# README #

# To start up: #
## Setup config ##
1. *git config --global user.name "YOUR_USERNAME"*
2. *git config --global user.email "your_email_address@example.com"*
## Clone repository ##
* *git clone https://username@bitbucket.org/smoothtalkers/speechtherapyapplication.git*
* This link is found on the overview page of SpeechTherapyApplication with your username

## Setting up Development Environment (Java): ##
* This program uses the NetBeans IDE. To download the IDE and the Java Development Kit, go to this [link](http://www.oracle.com/technetwork/articles/javase/jdk-netbeans-jsp-142931.html).
* Goto: File > Open Project... and find java_program/SpeechTherapyApplication. This should open as a Maven project.
* Run the project and it should download the required dependencies automatically.

# Development Cycle: #
## Create a branch ##
* *git checkout -b branch-name*
## Develop on branch ##
* Make Commits with meaningful information
* Make sure to rebase with master branch (especially before merging)
    * *git fetch && git rebase master*
## Squash and merge branch to master ##
1. *git push origin branch-name*
2. Create a pull request on BitBucket
3. Wait for review by everyone
    * Any issues here should be handled with new commits
4. Merge pull request
    * Merge strategy: Squash
    * Summarize the addition or fix.

# How to work on fixing issues: #
1. Create a branch with that issue number and shorten name
    * *git checkout -b 1-failed-save*
2. Follow development cycle
    * Explain what you did in comments on issue tracker (bookkeeping)
    * See [this](https://confluence.atlassian.com/bitbucket/resolve-issues-automatically-when-users-push-code-221451126.html) to automatically close issue

# For the test program in python #
## Dependencies ##
* [Python 2.7.13](https://www.python.org/downloads/release/python-2713/) (Possible future change to 3.6)
* Type *pip install -r requirements.txt*
* Pocketsphinx
    * *pip install pocketsphinx*
    * A phone dictionary must be added (found in model directory) to pocketsphinx module
        * Location: C:\Python27\Lib\site-packages\pocketsphinx\model
        * Phone Dictionary: model\en-us-phone.lm.bin
* PyAudio
    * *pip install pyaudio*
* wxPython
    * *pip install wxPython*

# Test Program #
* This is the initial test of pocketsphinx and phone recognition
* To begin, type: *python test_program/application.py*
* Instructions
    * Record
        * Begin a recording
    * Stop
        * Stop the recording
    * Analyze
        * Begin analyzing last recording
        * Be patient, this will take a while
    * Replay
        * Replay last recording
    * Clear
        * Clear phones
