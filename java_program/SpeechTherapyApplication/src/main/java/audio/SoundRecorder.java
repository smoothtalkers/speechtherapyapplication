package audio;

import javax.sound.sampled.*;
import java.io.*;
import javax.swing.JSlider;
import audio.wavfile.FileHolder;
import java.util.ArrayList;

/**
 * A class for recording and playback of audio
 */
public class SoundRecorder {
    /**
     * An enum to keep track of the state of this class.
     */
    public enum Recorder {
        /**
         * State for no current activity
         */
        NORMAL,
        /**
         * State for playing audio
         */
        PLAYING,
        /**
         * State for recording
         */
        RECORDING
    }
    /**
     * File for all recording
     */
    String audioDataPath;
    
    /**
     * File for recording wave files into
     */
    private File wavFile;
    
    /**
     * Format of audio file type
     */
    AudioFileFormat.Type fileType = AudioFileFormat.Type.WAVE;
    
    /**
     * the line from which audio data is captured
     */
    TargetDataLine line;
    
    /**
     * An instance of the enum to keep track of playback
     */
    Recorder state;
    
    /**
     * Object to hold current audio file and minim players.
     */
    FileHolder fileHolder;
    
    /**
     * Generates the desired audio format for speech recognition
     * @return desired audio format
     */
    private AudioFormat getAudioFormat() {
        float sampleRate = 16000;
        int sampleSizeInBits = 16;
        int channels = 1;
        boolean signed = true;
        boolean bigEndian = false;
        AudioFormat format = new AudioFormat(sampleRate, sampleSizeInBits,
                                             channels, signed, bigEndian);
        return format;
    }
    
    /**
     * Accessing function for wavFile
     * @return Last recorded wave file
     */
    public File getWaveFile() {
        return wavFile;
    }

    /**
     * Accessing function for setting wavFile
     * @param file new file
     */
    public void setWaveFile(File file) {
        wavFile = file;
    }
    
    /**
     * Plays the last recording created
     * @param scrubbers List of JSliders used for scrubbing
     * @param startFrame Frame to start playback at
     * @param endFrame Frame to end playback at
     */
    public void playRecording(ArrayList<JSlider> scrubbers, int startFrame, int endFrame) {
        if (state != Recorder.NORMAL || wavFile == null)
            return;
        else
            state = Recorder.PLAYING;

        // Setup input stream
        AudioInputStream audioInputStream;
        try {
            audioInputStream = AudioSystem.getAudioInputStream(wavFile);
        } catch (UnsupportedAudioFileException | IOException exc) {
            state = Recorder.NORMAL;
            return;
        }
        
        // Setup clip
        Clip clip;
        try {
            clip = AudioSystem.getClip();
            clip.open(audioInputStream);
        } catch (LineUnavailableException | IOException exc) {
            state = Recorder.NORMAL;
            return;
        }
        
        // Use a thread to avoid halting the system while audio plays
        Thread playClip = new Thread(() -> {
                clip.setFramePosition(startFrame);
                clip.start();
                long totalFrames = clip.getFrameLength();
                while (endFrame > clip.getLongFramePosition() && totalFrames > clip.getLongFramePosition()) {
                    scrubbers.forEach((scrubber) -> {
                        scrubber.setValue((int) clip.getLongFramePosition());
                    });
                }
                clip.stop();
                scrubbers.forEach((scrubber) -> {
                    scrubber.setValue(endFrame);
            });

                state = Recorder.NORMAL;
            });
        playClip.start();
        
    }
    
    /**
     * Captures the sound and records it into a WAV file
     * This is non-blocking and can be stopped by the GUI
     * @param folderName Directory for specific recording. Default is the current prompt 
     */
    public void startRecording(String folderName) {
        if (state != Recorder.NORMAL) {
            return;
        }
        
        // Prep folderName for creating a folder
        if ("Click \"Next\" to get a new prompt...".equals(folderName)) {
            folderName = "default";
        }
        else {
            folderName = folderName.toLowerCase();
            folderName = folderName.replace(' ', '_');
        }
        File dir = new File(audioDataPath + folderName);
        dir.mkdir();
        
        // Create a file for the audio. "next number".wav is the current syntax
        int extension = 0;
        do {
            String fileName = extension + ".wav";
            if (!System.getProperty("os.name").startsWith("Windows")) {
                wavFile = new File(dir.getAbsolutePath() + "/" + fileName);
            }
            else {
                wavFile = new File(dir.getAbsolutePath() + "\\" + fileName);
            }
            extension++;
        } while (wavFile.exists());

        try {
            AudioFormat format = getAudioFormat();
            DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
            
            //Check for system support for the data line
            if (!AudioSystem.isLineSupported(info)) {
                // TODO: Add user feedback
                System.out.println("Line not supprted");
                return;
            }
            line = (TargetDataLine) AudioSystem.getLine(info);
            line.open(format);
            line.start();
            
            AudioInputStream ais = new AudioInputStream(line);
            
            state = Recorder.RECORDING;
            // To avoid blocking by the write, create a new thread
            // This is terminated upon closing of the line 
            Thread record = new Thread(() -> {
                try {
                    AudioSystem.write(ais, fileType, wavFile);
                } catch (IOException ioe) {
                    System.out.println("Error on write");
                    state = Recorder.NORMAL;
                }
            });
            record.start();
            
        } catch (LineUnavailableException ex) {
            state = Recorder.NORMAL;
        }
    }
    
    /**
     * Stops the current recording
     */
    public void stopRecording() {
        state = Recorder.NORMAL;
        if (line != null) {
            line.stop();
            line.close();
            fileHolder.update(wavFile);
        }
        fileHolder.update(wavFile);
    }
    
    /**
     * Main constructor for SoundRecorder
     * Creates a directory at the set audioDataPath
     * @param fileHolderParam File holder object
     */
    public SoundRecorder(FileHolder fileHolderParam) 
    {        
        if (!System.getProperty("os.name").startsWith("Windows")) {
            audioDataPath = "src/main/java/audio/data/";
        }
        else {
            audioDataPath = "src\\main\\java\\audio\\data\\";
        }
        
        File audioDataDir = new File(audioDataPath);
        audioDataDir.mkdir();
        state = Recorder.NORMAL;
        fileHolder = fileHolderParam;
    }
}
