package audio;

import java.awt.Graphics;
import audio.wavfile.*;
import javax.swing.JSlider;
import ddf.minim.*;
import ddf.minim.analysis.*;
import java.awt.BorderLayout;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 * Class used to create a JPanel that will display a FFT based on the wavfile
 * passed into it. This should be treated like a JPanel.
 */
public class FFTPanel extends javax.swing.JPanel {

    /**
     * A series to store the frequency and magnitude for graphing
     */
    XYSeries fftGraphData;

    /**
     * Used to display the fftGraphData on the graph
     */
    XYSeriesCollection chartData;

    /**
     * The chart object used to fill the panel, implemented as a line graph
     */
    JFreeChart chart;

    /**
     * The Panel to house the chart
     */
    ChartPanel chartPanel;

    /**
     * Object to hold current audio file and minim players.
     */
    FileHolder fileHolder;

    /**
     * Standard constructor for FFTPanel
     *
     * @param fileHolderParam Holds current audio file and minim players
     * @param slide JSlider for scrubbing
     */

    public FFTPanel(FileHolder fileHolderParam, JSlider slide) {

        fileHolder = fileHolderParam;
        fftGraphData = new XYSeries("Data");
        chartData = new XYSeriesCollection();
        chartData.addSeries(fftGraphData);
        chart = ChartFactory.createXYLineChart("FFT", "Frequency", "Magnitude", chartData, PlotOrientation.VERTICAL, true, true, true);
        chartPanel = new ChartPanel(chart);
    }

    /**
     * Update FFT with new audio file.
     */
    public void update() {
        AudioSample player = fileHolder.getAudioSample();
        // Find power of 2 one larger than the frames length
        int power = 0;
        fftGraphData.clear();
        while (Math.pow(2, power) < player.getChannel(AudioSample.LEFT).length) {
            power++;
        }

        // Generate FFT based on audio sample
        FFT fft = new FFT((int) Math.pow(2, power), player.sampleRate());
        float[] result = player.getChannel(AudioSample.LEFT);
        float[] powerA = new float[fft.timeSize()];
        System.arraycopy(result, 0, powerA, 0, result.length);
        fft.forward(powerA);


        for (int i = 0; i < 4000; i++) {
            fftGraphData.add(i*2, fft.getFreq(i*2));

        }
        repaint();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (fftGraphData != null) {
            this.setLayout(new BorderLayout());
            this.add(chartPanel);
            this.validate();
        }
    }
}
