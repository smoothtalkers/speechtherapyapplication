package audio;

import audio.wavfile.*;
import edu.cmu.sphinx.frontend.*;
import edu.cmu.sphinx.frontend.filter.Preemphasizer;
import edu.cmu.sphinx.frontend.transform.*;
import edu.cmu.sphinx.frontend.util.*;
import edu.cmu.sphinx.frontend.window.RaisedCosineWindower;
import edu.cmu.sphinx.frontend.frequencywarp.MelFrequencyFilterBank;
import graphics.GraphDrawer;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javax.swing.JSlider;


/**
 * Class used to create a JPanel that will display a FFT based on the wavfile passed into it.
 * This should be treated like a JPanel.
 */
public class SpectrogramPanel extends javax.swing.JPanel {
    /**
     * The slider used for scrubbing through frames
     */
    JSlider slider;
    
    /**
     * The object used to draw the graph based off of the given bands
     */
    GraphDrawer grapher;

    /**
     * Object to hold current audio file and minim players.
     */
    FileHolder fileHolder;
    
    /**
     * Buffered image to hold spectrogram
     */
    BufferedImage bufferedImage;
    
    /**
     * Object used to hold frontend for spectrogram generator
     */
    private final FrontEnd frontend;
    
    /**
     * Source of audio
     */
    private final AudioFileDataSource source;
        
    /**
     * Standard constructor for SpectrogramPanel
     * @param fileHolderParam Holds current audio file and minim players
     * @param slide JSlider for scrubbing
     */
    public SpectrogramPanel (FileHolder fileHolderParam, JSlider slide) {
        slider = slide;
        grapher = new GraphDrawer(this, GraphDrawer.Center.BOTTOM, 0, 100);
        fileHolder = fileHolderParam;
        
        // frontend for MFCC
        source = new AudioFileDataSource(320, null);
        RaisedCosineWindower windower = new RaisedCosineWindower(0.4f,
                25.625f, 10.0f);
        Preemphasizer p = new Preemphasizer();
        DiscreteFourierTransform fft = new DiscreteFourierTransform();
        MelFrequencyFilterBank filterbank = new MelFrequencyFilterBank(1.0,
                8000.0, 40);

        ArrayList<DataProcessor> list = new ArrayList<>();
        list.add(source);
        list.add(windower);
        list.add(fft);
        list.add(filterbank);
        
        frontend = new FrontEnd(list);
    }
    
    /**
     * Get the current scrubbing slider value. This is the index for bands[].
     * @return Current scrubbing slider value.
     */
    private int getStartingIdx() {
        float windowWidth = 1;
        if (bufferedImage != null)
            windowWidth = (float)(slider.getMaximum() - slider.getMinimum()) / (float)bufferedImage.getWidth();
        return (int) (slider.getValue() / windowWidth);
    }
    
    /**
     * Get the starting frame of the selection.
     * @return starting frame
     */
    public int getStartFrame() {
        return grapher.getStartSelection();
    }
    
    /**
     * Get the ending frame of the selection.
     * @return ending frame
     */
    public int getEndFrame() {
        return grapher.getEndSelection();
    }
    
    /**
     * Update FFT with new audio file.
     */
    public void update() {
        source.setAudioFile(fileHolder.getOriginalFile(), "");
        Data data;
        double maxIntensity = 0;
        
        ArrayList<double[]> values = new ArrayList<>();
        while ((data = frontend.getData()) != null) {
            if (data instanceof DoubleData) {

                //double maxIntensity = lowIntensity;
                double[] frame = ((DoubleData) data).getValues();
                double[] value = new double[frame.length];
                
                for (int i = 0; i < frame.length; i++) {
                    // Adjust the values for displaying in the spectrogram
                    value[i] = Math.max(Math.log(frame[i]), 0.0);
                    if (value[i] < 0)
                        value[i] = 0;
                    if (value[i] > 255)
                        value[i] = 255;
                    if (value[i] > maxIntensity)
                        maxIntensity = value[i];
                }
                values.add(value);
            }
        }
        
        int width = values.size();
        int height = (values.get(0)).length;
        int maxYIndex = height - 1;

        /* Create the image for displaying the data.
         */
        bufferedImage = new BufferedImage(width,
                height,
                BufferedImage.TYPE_INT_RGB);

        /* Set scaleFactor so that the maximum value, after removing
        * the offset, will be 0xff.
        */
        double scaleFactor = 255.0 / maxIntensity;

        for (int i = 0; i < width; i++) {
            double[] intensities = values.get(i);
            for (int j = maxYIndex; j >= 0; j--) {

                /* Adjust the grey value to make a value of 0 to mean
                * white and a value of 0xff to mean black.
                */
                int grey = (int) (intensities[j] * scaleFactor);
                grey = Math.max(grey, 0);
                grey = 0xff - grey;

                /* Turn the grey into a pixel value.
                */
                int pixel = ((grey << 16) & 0xff0000)
                        | ((grey << 8) & 0xff00)
                        | (grey & 0xff);

                bufferedImage.setRGB(i, maxYIndex - j, pixel);
            }
        }
        repaint();
    }

    /**
     * Sets the magnification for the graph. Passed to the grapher
     * @param m Magnification to be set
     */
    public void setMagnification(float m) {
        grapher.setMag(m);
    }

    /**
     * Gets the magnification for the graph.
     * @return Current magnification
     */
    public float getMagnification() {
        return grapher.getMag();
    }

    /**
     * Method to update the magnification of the graph.
     * Called every time the mouse wheel is moved on the JPanel.
     * @param e The MouseWheelEvent generated by the MouseWheelListener
     */
    public void mouseWheelMoved(MouseWheelEvent e) {
        if (e.getWheelRotation() < 0)
            grapher.changeMag(0.1f);
        else if (e.getWheelRotation() > 0)
            grapher.changeMag(-0.1f);
        repaint();
    }

    /**
     * Method to update the start of the selection. A right click causes the
     * selection to clear.
     * @param event The MouseEvent generated by the MouseListener
     */
    public void mousePressed(MouseEvent event) {
        int imageWidth = (int) (bufferedImage.getWidth());
        if (event.getButton() == java.awt.event.MouseEvent.BUTTON3) {
            grapher.setStartSelection(0, imageWidth);
            repaint();
            return;
        }
        grapher.setStartSelection(event.getX(), imageWidth, getStartingIdx());
        repaint();
    }

    /**
     * Method to update the end of the selection.
     * @param event The MouseEvent generated by the MouseMotionListener
     */
    public void mouseDragged(MouseEvent event) {
        int imageWidth = (int) (bufferedImage.getWidth());
        grapher.setEndSelection(event.getX(), imageWidth, getStartingIdx());
        repaint();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (bufferedImage != null)
            grapher.drawImage(g, bufferedImage, getStartingIdx(), true, true);
    }
}
