package audio.wavfile;

import ddf.minim.*;
import java.io.File;

/**
 * Singleton class for holding the current audio file and minim players
 */
public class FileHolder {
    /**
     * Minim for entry to the minim dependency
     */
    Minim minim;
    
    /**
     * This holds the original generate file for Sphinx4
     */
    File originalFile;
    
    /**
     * The audio sample from the current file. Produced by minim.
     */
    AudioSample audioSample;
    
    /**
     * The audio player from the current file. Produced by minim.
     */
    AudioPlayer audioPlayer;
    
    /**
     * Main constructor for FileHolder
     */
    public FileHolder() {
        minim = new Minim(new MinimConfig());
    }
    
    /**
     * Update the file, audio player, and audio sample with the provided file.
     * @param file Provided file
     */
    public void update(File file) {
        originalFile = file;
        audioPlayer = minim.loadFile(file.getAbsolutePath());
        audioSample = minim.loadSample(file.getAbsolutePath());
    }
    
    /**
     * Get original file
     * @return original file
     */
    public File getOriginalFile() {
        return originalFile;
    }
    
    /**
     * Get audio sample
     * @return audio sample
     */
    public AudioSample getAudioSample() {
        return audioSample;
    }
    
    /**
     * Get audio player
     * @return audio player
     */
    public AudioPlayer getAudioPlayer() {
        return audioPlayer;
    }
}
