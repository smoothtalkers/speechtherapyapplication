package audio.wavfile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * Class used to provide sketchPath and createInput for Minim.
 */
public class MinimConfig {
    /**
     * Return provided file name
     * @param fileName Provided file name
     * @return Provided file name
     */
    public String sketchPath( String fileName ) {
        return fileName;
    }
    
    /**
     * Create input stream from the file name.
     * @param fileName Provided file name
     * @return Input stream on the file
     */
    public InputStream createInput( String fileName ) {
        File f = new File(fileName);
        try {
        InputStream bla = new FileInputStream(f);
        return bla;
        } catch (FileNotFoundException e) {
            return null;
        }
        
    }
}
