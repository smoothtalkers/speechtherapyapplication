package text;

import java.io.*;
import java.util.List;
import java.util.ArrayList;
/**
 * A class for holding prompts. Allows for easy access to new prompts
 */
public class PromptLibrary {
    /**
     * List of prompts loaded from the text file
     */
    static List<String> prompts;
    /**
     * Current index in the prompts list
     */
    private int currentIndex;
    
    /**
     * Wraps currentIndex to the beginning or end of prompts list
     */
    private void wrapAround() {
        if (currentIndex < 0) {
            currentIndex = prompts.size() - 1;
        }
        else if (currentIndex >= prompts.size()) {
            currentIndex = 0;
        }
    }
    
    /**
     * Goes to the previous prompt.
     * If it is the beginning of the list, go to the end.
     * @return The previous prompt in the list
     */
    public String getPreviousPrompt() {
        currentIndex--;
        wrapAround();
        return prompts.get(currentIndex);
    }
    
    /**
     * Goes to the next prompt. 
     * If it is the end of the list, return to the start.
     * @return The next prompt in the list
     */
    public String getNextPrompt() {
        currentIndex++;
        wrapAround();
        return prompts.get(currentIndex);
    }
    
    /**
     * Takes in a file name and extracts each line as a separate prompt.
     * Puts each line in a list with some accessing functions
     * @param promptTextFile File name of prompts text file
     */
    public PromptLibrary(String promptTextFile) {
        try {
            prompts = new ArrayList<>();
            
            File sourceFile;
            
            if (!System.getProperty("os.name").startsWith("Windows")) {
                sourceFile = new File("src/main/java/text/data/" + promptTextFile);
            }
            else {
                sourceFile = new File("src\\main\\java\\text\\data\\" + promptTextFile);
            }
            try (FileReader fileReader = new FileReader(sourceFile)) {
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    prompts.add(line);
                }
            }
            currentIndex = 0;
        } catch (IOException ioe) {
            System.out.println("Failed to open or read file.");
        }
    }
}
