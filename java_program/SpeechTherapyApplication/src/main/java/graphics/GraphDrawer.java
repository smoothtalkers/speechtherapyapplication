package graphics;

import java.awt.Graphics;
import java.awt.Color;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

/**
 * This is a class to help facilitate the drawing of graphs using data.
 */
public class GraphDrawer {
    /**
     * Enum for the location of the x-axis on the screen
     */
    public static enum Center {

        /**
         * Place the x-axis at the top of the screen
         */
        TOP, 
        /**
         * Place the x-axis at the middle of the screen
         */
        MIDDLE, 
        /**
         * Place the x-axis at the bottom of the screen
         */
        BOTTOM
    }
      
    /**
     * The JPanel to draw in
     */
    JPanel window;
    
    /**
     * The current magnification level
     */
    float mag;
    
    /**
     * The start of the selection range
     */
    int startSelection;
    
    /**
     * The end of the selection range
     */
    int endSelection;
    
    /**
     * An enum of where to draw the x axis
     */
    Center center;
    
    /**
     * The minimum possible value in the data array
     */
    float minValue;
    
    /**
     * The maximum possible value in the data array
     */
    float maxValue;
    
    /**
     * The main constructor for the Graph Drawer
     * @param panel JPanel that is being drawn in
     * @param cent Desired center position for x axis
     * @param min Minimum possible value in data
     * @param max Maximum possible value in data
     */
    public GraphDrawer(JPanel panel, Center cent, int min, int max) {
        window = panel;
        center = cent;
        if (center == Center.TOP)
            max = 0;
        if (center == Center.BOTTOM)
            min = 0;
        mag = 1.0f;
        startSelection = 0;
        endSelection = 0;
        minValue = min;
        maxValue = max;
    }
    
    /**
     * Sets the magnification. Limits the magnification between 0.1 and 10.0
     * @param newMag New magnification to set mag to
     */
    public void setMag(float newMag) {
        if (newMag > 10.0f)
            mag = 10.0f;
        else if (newMag < 0.1f)
            mag = 0.1f;
        else
            mag = newMag;
    }
    
    /**
     * Get current magnification
     * @return Current magnification
     */
    public float getMag() {
        return mag;
    }
    
    /**
     * Changes the current magnification by 'change'
     * @param change Amount to change mag by
     */
    public void changeMag(float change) {
        setMag(mag + change);
    }
    
    /**
     * Sets the start index for selection based off the selected pixel. If too
     * small or too big, it sets it to 0 or the end of the array respectively. 
     * @param pixel Selected pixel to convert and set to start
     * @param dataLength Length of data array
     * @param currentValueIdx The index of the current value. Used to find the windows position
     */
    public void setStartSelection(int pixel, int dataLength, int currentValueIdx) {
        int index = getIdxAtPixel(dataLength, currentValueIdx, pixel);
        setStartSelection(index, dataLength);
    }
    
    /**
     * Sets the start index for selection. Also sets the end index to the last
     * index of the data array. If too small or too big, it sets it to 0 or the 
     * end of the array respectively.
     * @param index Index to set as the start
     * @param dataLength Length of data array
     */
    public void setStartSelection(int index, int dataLength) {
        startSelection = index;
        if (startSelection >= dataLength)
            startSelection = dataLength - 1;
        if (startSelection < 0)
            startSelection = 0;
        endSelection = dataLength - 1;
    }
    
    /**
     * Get the start index for selection
     * @return Start index of selection
     */
    public int getStartSelection() {
        return startSelection;
    }
    
    /**
     * Sets the end index for selection based off the selected pixel. If too
     * small or too big, it sets it to 0 or the end of the array respectively.
     * @param pixel Selected pixel to convert and set to end
     * @param dataLength Length of data array
     * @param currentValueIdx The index of the current value. Used to find the windows position
     */
    public void setEndSelection(int pixel, int dataLength, int currentValueIdx) {
        int index = getIdxAtPixel(dataLength, currentValueIdx, pixel);
        setEndSelection(index, dataLength);
    }
    
    /**
     * Sets the end index for selection. If too small or too big, it sets it to 
     * 0 or the end of the array respectively.
     * @param index Index to set as the end
     * @param dataLength Length of data array
     */
    public void setEndSelection(int index, int dataLength) {
        endSelection = index;
        if (endSelection >= dataLength)
            endSelection = dataLength - 1;
        if (endSelection < 0)
            endSelection = 0;
    }
    
    /**
     * Get the end index for selection
     * @return End index of selection
     */
    public int getEndSelection() {
        return endSelection;
    }
    
    /**
     * Return the value normalized between a max and a minimum number.
     * This works around a specific center as well
     * @param value Value to be normalized
     * @return The normalized value
     */
    private float getNormalizedValue(float value) {
        float correctValue = value - minValue;
        float targetRange = 1;
        if (center == Center.MIDDLE)
            targetRange = 2;
        correctValue *= targetRange;
        correctValue /= maxValue - minValue;
        float targetMinValue = -1;
        if (center == Center.BOTTOM)
            targetMinValue = 0;
        correctValue += targetMinValue;
        return correctValue;
    }
    
    /**
     * Draw a vertical line on a specific data value
     * @param g Graphics to draw with
     * @param currentValueIdx The index of the current value. Used to find the windows position
     * @param dataLength Length of the data array used for drawGraph
     */
    private void drawStartingLine(Graphics g, int currentValueIdx, int dataLength) {
        // Gather screen info from JPanel
        float screenWidth = window.getWidth();
        float graphWidth = screenWidth * mag;
        float pixelsPerValue = (graphWidth) / (float) dataLength;
        
        g.setColor(Color.BLACK);
        
        // Set the correct vertical line drawing
        int verticalLinePixel;
        if (screenWidth >= graphWidth)
            verticalLinePixel = (int) (currentValueIdx * pixelsPerValue);
        else if (currentValueIdx * pixelsPerValue < (screenWidth / 2.0f))
            verticalLinePixel = (int) (currentValueIdx * pixelsPerValue);
        else if (currentValueIdx * pixelsPerValue > graphWidth - screenWidth / 2.0f) {
            float graphOffset = graphWidth - screenWidth;
            verticalLinePixel = (int) (currentValueIdx * pixelsPerValue - graphOffset);
        }
        else
            verticalLinePixel = (int) (screenWidth / 2.0f);

        g.drawLine(verticalLinePixel, 0, verticalLinePixel, window.getHeight());
    }
    
    /**
     * Helper method to find the index at the left-most side of the graph
     * @param currentValueIdx The index of the current value. Used to find the windows position
     * @param screenWidth The width of the window
     * @param graphWidth The width of the entire graph
     * @param pixelsPerValue The amount of pixels to be covered by a single value
     * @return 
     */
    private int getActualStart(int currentValueIdx, float screenWidth, float graphWidth, float pixelsPerValue) {
        int actualStart = currentValueIdx;
        if (screenWidth >= graphWidth)
            actualStart = 0;
        else if ((float)actualStart * pixelsPerValue < (screenWidth / 2.0f))
            actualStart = 0;
        else if ((float)actualStart * pixelsPerValue > graphWidth - screenWidth / 2.0f)
            actualStart = (int) ((graphWidth - screenWidth) / pixelsPerValue);
        else
            actualStart -= (int) ((screenWidth / 2.0f) / pixelsPerValue);
        return actualStart;
    }
    
    /**
     * Converts a given pixel to the data array index.
     * @param dataLength Length of data array
     * @param currentValueIdx The index of the current value. Used to find the windows position
     * @param pixel Pixel to convert
     * @return The resulting data array index
     */
    private int getIdxAtPixel(int dataLength, int currentValueIdx, int pixel) {
        // Gather screen info from JPanel
        float screenWidth = window.getWidth();
        float graphWidth = screenWidth * mag;
        
        // Calculate the actual start to the drawing for smooth scrolling
        float pixelsPerValue = graphWidth / (float) dataLength;
        int actualStart = getActualStart(currentValueIdx, screenWidth, graphWidth,
                pixelsPerValue);
        
        int idxFromPixel = (int) ((float) pixel / pixelsPerValue);
        return actualStart + idxFromPixel;
    }
    
    /**
     * Draws a range selection (a filled rectangle) on the graph.
     * @param g Graphics to draw with
     * @param dataLength Length of the data array
     * @param currentValueIdx The index of the current value. Used to find the windows position
     */
    private void drawRangeSelection(Graphics g, int dataLength, int currentValueIdx) {
        if (startSelection == 0 && endSelection == dataLength - 1)
            return;
         // Gather screen info from JPanel
        float screenWidth = window.getWidth();
        float graphWidth = screenWidth * mag;
        
        // Calculate the actual start to the drawing for smooth scrolling
        float pixelsPerValue = graphWidth / (float) dataLength;
        int actualStart = getActualStart(currentValueIdx, screenWidth, graphWidth,
                pixelsPerValue);
        
        int startInPixels = (int) ((float) startSelection * pixelsPerValue);
        int endInPixels = (int) ((float) endSelection * pixelsPerValue);
        int actualInPixels = (int) ((float) actualStart * pixelsPerValue);
        
        int startRect = startInPixels - actualInPixels;
        int endRect = endInPixels - actualInPixels;
        Color fade = new Color(0, 255, 255, 50);
        g.setColor(fade);
        g.fillRect(startRect, 0, endRect - startRect, window.getHeight());
        g.setColor(Color.BLUE);
        g.drawLine(startRect, 0, startRect, window.getHeight());
        g.drawLine(endRect, 0, endRect, window.getHeight());
    }
    
    /**
     * Draw a graph based off of the data given to it.
     * @param g Graphics to draw with
     * @param data Array of all data
     * @param currentValueIdx The index of the current value. Used to find the windows position
     */
    private void drawGraph(Graphics g, float[] data, int currentValueIdx) {
        // Gather screen info from JPanel
        float screenWidth = window.getWidth();
        float graphWidth = screenWidth * mag;
        int height = window.getHeight();
        
        g.setColor(Color.BLACK);
        
        // Set up y = mx + b equation to handle varying center positions
        float slope = 0;
        float b = 0;
        if (null != center)
            switch (center) {
                case TOP:
                    slope = -height;
                    break;
                case MIDDLE:
                    slope = -height / 2;
                    b = height / 2;
                    break;
                case BOTTOM:
                    slope = -height;
                    b = height;
                    break;
                default:
                    break;
            }
        
        // Calculate the actual start to the drawing for smooth scrolling
        float pixelsPerValue = (graphWidth) / (float) data.length;
        int actualStart = getActualStart(currentValueIdx, screenWidth, graphWidth,
                pixelsPerValue);
        
        // Setup previous values for seamless scrolling
        float prevPixelIdx = (float) actualStart * pixelsPerValue;
        float prevValue = slope * getNormalizedValue(data[actualStart]) + b;
        int pixelOffset = (int) (actualStart * pixelsPerValue);

        for (int valueIdx = actualStart; valueIdx < data.length; valueIdx++) {
            float currValue = slope * getNormalizedValue(data[valueIdx]) + b;
            g.drawLine((int) prevPixelIdx - pixelOffset, (int) prevValue,
                   (int) (pixelsPerValue + prevPixelIdx) - pixelOffset, (int) currValue);
            prevValue = currValue;
            prevPixelIdx += pixelsPerValue;
            // Quit drawing once out of screen to avoid expensive functions
            if (prevPixelIdx > pixelOffset + screenWidth)
                break;
        }
    }
    
    /**
     * Draw a graph based off of the data given to it.
     * @param g Graphics to draw with
     * @param data Array of all data
     * @param currentValueIdx The index of the current value. Used to find the windows position
     */
    private void draw2DGraph(Graphics g, float[][] data, int currentValueIdx) {
        // Gather screen info from JPanel
        float screenWidth = window.getWidth();
        float graphWidth = screenWidth * mag;
        int height = window.getHeight();
        int spectrogramHeight = data[0].length;
        float valuesPerYAxis = (float) spectrogramHeight / (float) height;
        
        g.setColor(Color.BLACK);
        
        // Calculate the actual start to the drawing for smooth scrolling
        float pixelsPerValue = (graphWidth) / (float) data.length;
        int actualStart = getActualStart(currentValueIdx, screenWidth, graphWidth,
                pixelsPerValue);
        
        // Setup previous values for seamless scrolling
        float currPixelIdx = (float) actualStart * pixelsPerValue;
        int pixelOffset = (int) (actualStart * pixelsPerValue);
        int size = (int) Math.ceil(valuesPerYAxis);
        for (int valueIdx = actualStart; valueIdx < data.length; valueIdx++) {
            
            for (int yPixel = 0; yPixel < height - 1; yPixel++) {
                float startIdx = yPixel * valuesPerYAxis;
                float total = 0;
                for (int i = 0; i < size; i++)
                    total += data[valueIdx][i + (int) startIdx];
                float average = total / (float)size;
                
                int color = (int) (255.0 - 20.0 * 2.0 * Math.max(0, Math.log10(average * 1000.0)));
                if (color < 0) color = 0;
                if (color > 255) color = 255;
                g.setColor(new Color(color, color, color));
                g.drawLine((int) currPixelIdx, yPixel, (int) currPixelIdx, yPixel);
            }
            currPixelIdx += pixelsPerValue;
            // Quit drawing once out of screen to avoid expensive functions
            if (currPixelIdx > pixelOffset + screenWidth)
                break;
        }
    }
    
    /**
     * The main method for drawing the graph. The boolean options add different 
     * items to be drawn on the graph.
     * @param g Graphics to draw with
     * @param data Array of all data
     * @param currentValueIdx The index of the current value. Used to find the windows position
     * @param drawRange True if the selection is to be drawn on the graph
     * @param drawCurrentValueLine True if the current value is to be drawn on the graph
     */
    public void drawWindow(Graphics g, float[] data, int currentValueIdx, boolean drawRange, boolean drawCurrentValueLine) {
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, window.getWidth(), window.getHeight());
        drawGraph(g, data, currentValueIdx);
        if (drawRange)
            drawRangeSelection(g, data.length, currentValueIdx);
        if (drawCurrentValueLine)
            drawStartingLine(g, currentValueIdx, data.length);
    }

    /**
     * The main method for drawing the graph from an image. The boolean options 
     * add different items to be drawn on the graph.
     * @param g Graphics to draw with
     * @param bufferedImage Image to draw
     * @param currentValueIdx The index of the current value. Used to find the windows position
     * @param drawRange True if the selection is to be drawn on the graph
     * @param drawCurrentValueLine True if the current value is to be drawn on the graph
     */
    public void drawImage (Graphics g, BufferedImage bufferedImage, int currentValueIdx, boolean drawRange, boolean drawCurrentValueLine) {
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, window.getWidth(), window.getHeight());
        float pixelsPerWindow = (float)window.getWidth() * mag / (float)bufferedImage.getWidth();
        int start = getActualStart(currentValueIdx, window.getWidth(), window.getWidth() * mag, pixelsPerWindow);
        g.drawImage(bufferedImage, (int)(-start * pixelsPerWindow), 0, (int) (window.getWidth() * mag), window.getHeight(), null);
        if (drawRange)
            drawRangeSelection(g, bufferedImage.getWidth(), currentValueIdx);
        if (drawCurrentValueLine)
            drawStartingLine(g, currentValueIdx, bufferedImage.getWidth());
    }
}
