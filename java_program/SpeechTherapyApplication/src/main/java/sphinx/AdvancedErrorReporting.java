package sphinx;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import javax.swing.DefaultListModel;

/**
 * provides advanced error reporting functionalities
 */
public class AdvancedErrorReporting {
    
    private static final String[] PHONES = {"AA", "AE", "AH", "AO", "AW", "AX", "AY", "B", "CH", "D", "DH", "EH", "ER", "EY", "F", "G", "HH", "IH", "IY", "JH", "K", "L", "M", "N", "NG", "OW", "OY", "P", "R", "S", "SH", "T", "TH", "UH", "UW", "V", "W", "Y", "Z", "ZH", "$"};

    private static final Map<String, Integer> MAP;
    static {
        MAP = new HashMap<>();
        for (int i = 0; i < PHONES.length; i++)
            MAP.put(PHONES[i], i);
    }
    
    /**
     * Find scores for pairs of phones
     * @param index index of phone being analyzed
     * @return list to be displayed to the user
     */
    public static DefaultListModel<String> diphones( int index ) {
        DefaultListModel<String> listModel = new DefaultListModel<>();
        
        BufferedReader fileReader = null;
        
        int[] truePositivePreceding = new int[PHONES.length];
        int[] falsePositivePreceding = new int[PHONES.length];
        int[] falseNegativePreceding = new int[PHONES.length];
        int[] truePositiveSubsequent = new int[PHONES.length];
        int[] falsePositiveSubsequent = new int[PHONES.length];
        int[] falseNegativeSubsequent = new int[PHONES.length];
        
        File table = new File("src\\main\\java\\sphinx\\data\\errortable.csv");
        if (!System.getProperty("os.name").startsWith("Windows")) {
            table = new File("src/main/java/sphinx/data/errortable.csv");
        }
        
        if (table.exists()) {
            String line = "";
            try {
                if (System.getProperty("os.name"). startsWith("Windows")) {
                    fileReader = new BufferedReader(new FileReader("src\\main\\java\\sphinx\\data\\errortable.csv"));
                }
                else {
                    fileReader = new BufferedReader(new FileReader("src/main/java/sphinx/data/errortable.csv"));
                }
            } catch (FileNotFoundException e) {
                System.out.println("error: table does not exist");
                System.exit(0);
            }
            try {
                fileReader.readLine();
                while ((line = fileReader.readLine()) != null) {
                    String[] tokens = line.split(",");
                    if (tokens.length < 3) {
                        System.out.println("wrong length " + tokens.length);
                    }
                    if (tokens[3].compareTo("fn") == 0 && tokens[0].compareTo(PHONES[index]) == 0){
                        falseNegativePreceding[MAP.get(tokens[1])]++;
                        falseNegativeSubsequent[MAP.get(tokens[2])]++;
                    }
                    if (tokens[3].compareTo("fp") == 0 && tokens[0].compareTo(PHONES[index]) == 0){
                        falsePositivePreceding[MAP.get(tokens[1])]++;
                        falsePositiveSubsequent[MAP.get(tokens[2])]++;
                    }
                    if (tokens[3].compareTo("tp") == 0 && tokens[0].compareTo(PHONES[index]) == 0){
                        truePositivePreceding[MAP.get(tokens[1])]++;
                        truePositiveSubsequent[MAP.get(tokens[2])]++;
                    }
                }
            } catch(IOException e) {
                System.out.println("error reading table");
            }
        }
        
        listModel.addElement("<- go back");
        
        for (int i = 0; i < PHONES.length-1; i++) {
            double score_pre = f1score(truePositivePreceding[i], falsePositivePreceding[i], falseNegativePreceding[i]);
            double score_post = f1score(truePositiveSubsequent[i], falsePositiveSubsequent[i], falseNegativeSubsequent[i]);
            DecimalFormat decimalFormat = new DecimalFormat("0.##");
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("<html><pre>");
            if (score_pre == -1) {
                stringBuilder.append(String.format("%s+%s\tno data\t\t", PHONES[i], PHONES[index]));
            }
            else {
                stringBuilder.append(String.format("%s+%s\t%s%s\t\t", PHONES[i], PHONES[index], decimalFormat.format(score_pre*100), "%"));
            }
            if (score_post == -1) {
                stringBuilder.append(String.format("%s+%s\tno data", PHONES[index], PHONES[i]));
            }
            else {
                stringBuilder.append(String.format("%s+%s\t%s%s", PHONES[index], PHONES[i], decimalFormat.format(score_post*100), "%"));
            }
            stringBuilder.append("</pre></html>");
            listModel.addElement(stringBuilder.toString());
        }
        
        //end of line/beginning of line scores
        int i = PHONES.length-1;
        double score_pre = f1score(truePositivePreceding[i], falsePositivePreceding[i], falseNegativePreceding[i]);
        double score_post = f1score(truePositiveSubsequent[i], falsePositiveSubsequent[i], falseNegativeSubsequent[i]);
        DecimalFormat df = new DecimalFormat("0.##");
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<html><pre>");
        if (score_pre == -1) {
            stringBuilder.append(String.format("%s at end of line: no data\t", PHONES[index]));
        }
        else {
            stringBuilder.append(String.format("%s at end of line %s%s\t", PHONES[index], df.format(score_pre*100), "%"));
        }
        if (score_post == -1) {
            stringBuilder.append(String.format("%s at beginning of line: no data", PHONES[index]));
        }
        else {
            stringBuilder.append(String.format("%s at beginning of line %s%s", PHONES[index], df.format(score_post*100), "%"));
        }
        stringBuilder.append("</pre></html>");
        listModel.addElement(stringBuilder.toString());
        return listModel;
    }
   
    /**
     * find scores for each phone
     * @return list to be displayed to the user
     */
    public static DefaultListModel<String> listcontents() {
        DefaultListModel<String> listModel = new DefaultListModel<>();
        
        BufferedReader fileReader = null;
        
        int[] truePositive = new int[PHONES.length];
        int[] falsePositive = new int[PHONES.length];
        int[] falseNegative = new int[PHONES.length];        
        
        File table = new File("src\\main\\java\\sphinx\\data\\errortable.csv");
        if (!System.getProperty("os.name").startsWith("Windows")) {
            table = new File("src/main/java/sphinx/data/errortable.csv");
        }
        
        if (table.exists()) {
            String line = "";
            try {
                if (System.getProperty("os.name"). startsWith("Windows")) {
                    fileReader = new BufferedReader(new FileReader("src\\main\\java\\sphinx\\data\\errortable.csv"));
                }
                else {
                    fileReader = new BufferedReader(new FileReader("src/main/java/sphinx/data/errortable.csv"));
                }
            } catch (FileNotFoundException e) {
                System.out.println("error: table does not exist");
                System.exit(0);
            }
            try {
                fileReader.readLine();
                while ((line = fileReader.readLine()) != null) {
                    String[] tokens = line.split(",");
                    if (tokens.length < 3) {
                        System.out.println("wrong length " + tokens.length);
                    }
                    if (tokens[3].compareTo("fn") == 0) {
                        falseNegative[MAP.get(tokens[0])]++;
                    }
                    if (tokens[3].compareTo("fp") == 0) {
                        falsePositive[MAP.get(tokens[0])]++;
                    }
                    if (tokens[3].compareTo("tp") == 0) {
                        truePositive[MAP.get(tokens[0])]++;
                    }
                }
            } catch(IOException e) {
                System.out.println("error reading table");
            }
        }
        
        listModel.addElement("F1 scores (click for more information)");
        
        for (int i = 0; i < PHONES.length-1; i++) {
            double score = f1score(truePositive[i], falsePositive[i], falseNegative[i]);
            DecimalFormat df = new DecimalFormat("0.##");
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("<html><pre>");
            if (score == -1) {
                stringBuilder.append(String.format("%s\tno data", PHONES[i]));
            }
            else {
                stringBuilder.append(String.format("%s\t%s%s", PHONES[i], df.format(score*100), "%"));
            }
            stringBuilder.append("</pre></html>");
            listModel.addElement(stringBuilder.toString());
        }
        
        return listModel;
    }
    
    /**
     * get f1scores
     */
    private static double f1score(int tp, int fp, int fn) {
        if (fp == 0 && fn == 0 && tp == 0) {
            return -1;
        }
        double falsePositive = fp;
        double truePositive = tp;
        double falseNegative = fn;
        return ((2*truePositive)/(2*truePositive+falseNegative+falsePositive));
    }
}