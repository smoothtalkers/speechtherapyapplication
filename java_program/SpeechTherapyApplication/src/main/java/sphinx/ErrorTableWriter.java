package sphinx;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * class for managing error table
 */
public class ErrorTableWriter {
    /**
     * write error to csv file
     * @param phone
     * @param preceding
     * @param subsequent
     * @param elementType
     * @return 0 if successful, -1 if failed
     */
    public static int write(String phone, String preceding, String subsequent, String elementType) {
        if (phone.length() == 0) {
            System.out.println("phone does not exist");
            return -1;
        }
        
        File table;
        
        if (!System.getProperty("os.name").startsWith("Windows")) {
            table = new File("src/main/java/sphinx/data/errortable.csv");
        }
        else {
            table = new File("src\\main\\java\\sphinx\\data\\errortable.csv");
        }

        FileWriter fileWriter;
        boolean newFile = false;
        if (!table.exists()) {
            try {
                File dir = new File("src\\main\\java\\sphinx\\data");
                
                if (!System.getProperty("os.name").startsWith("Windows")) {
                    dir = new File("src/main/java/sphinx/data");                    
                }
                
                dir.mkdir();
                table.createNewFile();                
            } catch (IOException e) {
                System.out.println("error creating new file");
                return -1;
            }
            newFile = true;
        }
        try {
            fileWriter = new FileWriter("src\\main\\java\\sphinx\\data\\errortable.csv", true);
            if (!System.getProperty("os.name").startsWith("Windows")) {
                fileWriter = new FileWriter("src/main/java/sphinx/data/errortable.csv", true);                
            }
            
            if (newFile) {
                fileWriter.append("phone,preceding,subsequent,elementtype\n");
            }
            fileWriter.append(phone);
            fileWriter.append(",");
            fileWriter.append(preceding);
            fileWriter.append(",");
            fileWriter.append(subsequent);
            fileWriter.append(",");
            fileWriter.append(elementType);
            fileWriter.append("\n");
            fileWriter.close();
        } catch (IOException e){
            System.out.println("FileWriter error");
            return -1;
        }
        return 0;
    }
}
