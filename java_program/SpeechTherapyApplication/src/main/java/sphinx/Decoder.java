package sphinx;

import java.io.*;
import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.Context;
import edu.cmu.sphinx.api.SpeechResult;
import edu.cmu.sphinx.recognizer.Recognizer;
import edu.cmu.sphinx.result.Result;
import edu.cmu.sphinx.util.TimeFrame;

import com.sun.speech.freetts.en.us.CMULexicon;
import audio.wavfile.FileHolder;

/**
 * A class to take audio files and produce speech recognition results
 */
public class Decoder {
    
    /**
     * Configuration for the speech recognition context
     */
    private Configuration configuration;
    
    /**
     * Context used to get a recognizer and set audio input
     */
    private Context context;
    
    /**
     * Lexicon for text to phones
     */
    private CMULexicon lexicon;
    
    /**
     * stores list of missed phones
     */
    private String[] missedPhones;
    
    /**
     * Object for holding current file and minim player.
     */
    FileHolder fileHolder;
    
    /**
     * Takes an audio file and returns the hypothesis phones for the audio
     * @param audioFile Audio file to be decoded
     * @return The hypothesis in phones for each word found by Sphinx
     */
    public String getPhonesHypothesis() {
        File audioFile = fileHolder.getOriginalFile();
        Recognizer recognizer = context.getInstance(Recognizer.class);
        InputStream stream = null;
        if (audioFile == null) {
            return "";
        }
        try {
            stream = new FileInputStream(audioFile.getAbsolutePath());
        } catch (FileNotFoundException fnfe) {
            return "";
        }

        // Decode phones from audio
        recognizer.allocate();
        context.setSpeechSource(stream, TimeFrame.INFINITE);
        Result result;
        String hypothesis = "";
        while ((result = recognizer.recognize()) != null) {
            System.out.println("Result: " + result);
            SpeechResult speechResult = new SpeechResult(result);
            hypothesis += speechResult.getHypothesis() + " ";
        }
        recognizer.deallocate();
        return hypothesis;
    }
    
    /**
     * Takes an audio file and returns the hypothesis phones for the audio
     * @param prompt Prompt to be translated to phones
     * @return A string of the phones from the prompt
     */
    public String getPhonesActual(String prompt) {
        String[] phones_list = lexicon.getPhones(prompt.toLowerCase(), null);

        // Strip off lexical stress numbers
        String result = "";
        for (int idx = 0; idx < phones_list.length; idx++) {
            phones_list[idx] = phones_list[idx].replaceAll("1", "");
            phones_list[idx] = phones_list[idx].replaceAll("2", "");
            result += phones_list[idx].toUpperCase();
            if (idx != phones_list.length - 1) {
                result += " ";
            }
        }
        
        return result;
    }
    
    /**
     * compare correct phone list to list derived from user audio
     * @param actual correct phone list
     * @param hypothesis phone list derived from user audio
     * @return accuracy score
     */
    public float getAccuracy(String actual, String hypothesis) {
        //we will define the accuracy as (n-x)/n where x is the edit distance and n is the length of the longer string
        
        //convert strings into arrays of phonemes
        String[] stringActual = actual.split(" ", 0);
        String[] stringHypothesis = hypothesis.split(" ", 0);
        
        int lengthActual = stringActual.length;
        int lengthHypothesis = stringHypothesis.length;
        
        for (int i = 0; i < lengthHypothesis; i++) {
            if (stringHypothesis[i].compareTo("SIL") == 0 || stringHypothesis[i].contains("+")) {
                for (int j = i; j < lengthHypothesis-1; j++)
                    stringHypothesis[j] = stringHypothesis[j+1];
                lengthHypothesis--;
                i--;
            }
        }
        
        //set up array to track phoneme errors
        boolean[] correct = new boolean[lengthActual];
        for (int i = 0; i < lengthActual; i++)
            correct[i] = false;
        int numFalse = lengthActual;
        
        //find edit distance using the Wagner-Fischer algorithm
        int[][] editArray;
        editArray = new int[lengthActual+1][lengthHypothesis+1];
        for (int i = 0; i <= lengthActual; i++) {
            editArray[i][0] = i;
        }
        for (int j = 0; j <= lengthHypothesis; j++) {
            editArray[0][j] = j;
        }
        for (int j = 1; j <= lengthHypothesis; j++) {
            for (int i = 1; i <= lengthActual; i++) {
                if (stringActual[i-1].compareTo(stringHypothesis[j-1]) == 0) {
                    editArray[i][j] = editArray[i-1][j-1];
                    
                    //record correct phoneme
                    if (correct[i-1] == false) {
                        correct[i-1] = true;
                        numFalse--;
                    }
                }
                else
                    editArray[i][j] = Math.min(Math.min(editArray[i-1][j]+1, editArray[i][j-1]+1), editArray[i-1][j-1]+1);
            }
        }
        float editDistance = editArray[lengthActual][lengthHypothesis];
        float len = Math.max(lengthActual,lengthHypothesis);
        
        float accuracy = (len-editDistance)/len;
        
        //identify true positives, false positives, and false negatives
        String[] str1 = new String[lengthActual+2];
        str1[0] = "$";
        System.arraycopy(stringActual, 0, str1, 1, lengthActual);
        str1[lengthActual+1] = "$";
        String[] str2 = new String[lengthHypothesis+2];
        str2[0] = "$";
        System.arraycopy(stringHypothesis, 0, str2, 1, lengthHypothesis);
        str2[lengthHypothesis+1] = "$";
        int x = lengthActual;
        int y = lengthHypothesis;
        while (x > 0 && y > 0) {
            if (x <= 0) {
                ErrorTableWriter.write(str2[y], str2[y-1], str2[y+1], "fp");
                y--;
            }
            else if (y <= 0) {
                ErrorTableWriter.write(str1[x], str1[x-1], str1[x+1], "fn");
                x--;
            }
            else if (editArray[x-1][y-1] <= editArray[x-1][y] && editArray[x-1][y-1] <= editArray[x][y-1]) {
                if (editArray[x-1][y-1] == editArray[x][y]) {
                    ErrorTableWriter.write(str1[x], str1[x-1], str1[x+1], "tp");
                }
                else {
                    ErrorTableWriter.write(str1[x], str1[x-1], str1[x+1], "fn");
                    ErrorTableWriter.write(str2[y], str2[y-1], str2[y+1], "fp");
                }
                x--;
                y--;
            }
            else if (editArray[x-1][y] <= editArray[x][y-1]) {
                if (editArray[x-1][y] == editArray[x][y])
                    ErrorTableWriter.write(str1[x], str1[x-1], str1[x+1], "tp");
                else
                    ErrorTableWriter.write(str1[x], str1[x-1], str1[x+1], "fn");  
                x--;
            }
            else {
                if (editArray[x][y-1] == editArray[x][y])
                    ErrorTableWriter.write(str1[x], str1[x-1], str1[x+1], "tp");
                else
                    ErrorTableWriter.write(str2[y], str2[y-1], str2[y+1], "fp");
                y--;
            }
        }
        
        missedPhones = new String[numFalse];
        
        int idx = 0;
        for (int i = 0; i < lengthActual; i++) {
            if (correct[i] == false) {
                missedPhones[idx] = stringActual[i];
                idx++;
            }
        }
        return accuracy;
    }
    
    /**
     * get list of missed phones
     * @return list of missed phones
     */
    public String getMissedPhones() {
        String list = "";
        for (String missedPhone : missedPhones) {
                    list += missedPhone;
                    list += " ";
        }
        
        return list;
    }
    
    /**
     * Main constructor that loads in acoustic model and dictionary
     * @param fileHolderParam holds files
     */
    public Decoder(FileHolder fileHolderParam) {
        fileHolder = fileHolderParam;
        
        configuration = new Configuration();

        // Load model from the jar
        if (!System.getProperty("os.name").startsWith("Windows")) {
            configuration
                    .setAcousticModelPath("src/main/resources/en-us-adapt");
        }
        else {
            configuration
                    .setAcousticModelPath("src\\main\\resources\\en-us-adapt");
        }

        configuration
                .setDictionaryPath("resource:/edu/cmu/sphinx/models/en-us/cmudict-en-us.dict");
        
        try {
            context = new Context(configuration);
        } catch (IOException ioe) {
            System.out.println("Failure to load configuration");
            return;
        }
        context.setLocalProperty("decoder->searchManager", "allphoneSearchManager");
        
        try {
            lexicon = CMULexicon.getInstance(true);
        } catch (IOException ioe) {
            System.out.println("Failure to generate lexicon.");
        }
        // Removes INFO output
//        Logger cmRootLogger = Logger.getLogger("default.config");
//        cmRootLogger.setLevel(java.util.logging.Level.OFF);
//        String conFile = System.getProperty("java.util.logging.config.file");
//        if (conFile == null) {
//            System.setProperty("java.util.logging.config.file", "ignoreAllSphinx4LoggingOutput");
//        }
    }
}
