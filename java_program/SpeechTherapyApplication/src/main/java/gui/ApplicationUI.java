package gui;

import audio.*;
import text.PromptLibrary;
import sphinx.AdvancedErrorReporting;
import sphinx.Decoder;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JSlider;
import javax.swing.JPanel;
import javax.swing.BorderFactory;
import javax.swing.border.TitledBorder;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import audio.wavfile.FileHolder;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import java.awt.Component;
import java.util.ArrayList;

/**
 * Main GUI and program
 */
public class ApplicationUI extends javax.swing.JFrame {
    
    /**
     * Class used for audio I/O
     */
    static SoundRecorder recorder;
    
    /**
     * Class used for gathering prompts
     */
    static PromptLibrary promptLibrary;
    
    /**
     * Class used for decoding audio files
     */
    static Decoder decoder;
    
    /**
     * Panel used for visualizing waveform
     */
    static WaveformPanel waveformPanel1;
    static WaveformPanel waveformPanel2;
    
    /**
     * Panel used for visualizing FFT
     */
    static FFTPanel fftPanel;
    
    /**
     * Slider used for scrubbing through waveform
     */
    static JSlider scrubberSlider1;
    static JSlider scrubberSlider2;
    
    /**
     * Object for holding current file and minim players
     */
    static FileHolder fileHolder;
    
    /**
     * Panel used for visualizing spectrogram
     */
    static SpectrogramPanel spectrogramPanel;

    /**
     * Creates new form ApplicationUI
     */
    public ApplicationUI() {
        initComponents();
        setupPanels();
    }
    
    /**
     * Function to setup all panels in all tabs
     */
    private void setupPanels() {
        setupWaveformPanels();
        setupFFTPanel();
        setupSpectrogramPanel();
        setupWaveformSpectroPanel();
        setupAudioAnalysisPanel();
        setupErrorReporting();
    }
    
    boolean onDiphoneView;
    
    /**
     * Method to set up the contents of the error reporting tab
     */
    private void setupErrorReporting(){
        onDiphoneView = false;
        
        //populate advanced error reporting tab
        advancedErrorList.setModel(AdvancedErrorReporting.listcontents());
        
        JFrame frame = new JFrame("About F1 scores");
        JPanel panel = new JPanel();
        LayoutManager layout = new GridLayout(2, 1, 0, 0);
        panel.setLayout(layout);
        panel.setBackground(Color.WHITE);
        JLabel f1ScoresImage = new JLabel(new ImageIcon("src\\main\\resources\\f1scores.png"));
        if (!System.getProperty("os.name").startsWith("Windows")) {
            f1ScoresImage = new JLabel(new ImageIcon("src/main/resources/f1scores.png"));
        }
        f1ScoresImage.setLocation(200, 400);
        panel.add(f1ScoresImage);
        JTextArea textArea = new JTextArea();
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        textArea.setEditable(false);
        textArea.setBorder(null);
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        textArea.setText("F1 scores are a measure of accuracy "
            + "that incorporate both precision (in this case, the proportion of phones pronounced that were supposed to be pronounced) "
            + "and recall (in this case, the proportion of phones that were supposed to be pronounced that were actually pronounced). "
            + "The formula for an F1 score is as follows:\n\n"
            + "(2*true positives)/(2*true positives + false positives + false negatives)\n\n"
            + "For our purposes, a true positive occurs when a a phone is pronounced correctly, "
            + "a false positive occurs when a phone is pronounced in the wrong place, "
            + "and a false negative occurs when the user fails to pronounce a phone. "
            + "This program calculates an F1 score for each phone, and can also break down each phone's score by which phones come before and after. "
            + "Click on an entry to see more details about that phone.");
        panel.add(textArea);
        frame.setSize(500, 600);
        frame.add(panel);
        
        advancedErrorList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {                
                JList errorReportingList = (JList)evt.getSource();
                int index = errorReportingList.locationToIndex(evt.getPoint());
                if (index == 0 && !onDiphoneView) {
                    frame.setVisible(true);
                }
                else if (index == 0 && onDiphoneView) {
                    errorReportingList.setModel(AdvancedErrorReporting.listcontents());
                    onDiphoneView = false;
                }
                else if (!onDiphoneView) {
                    errorReportingList.setModel(AdvancedErrorReporting.diphones(index-1));
                    onDiphoneView = true;
                }
            }
        });
    }
    
    /**
     * Method to setup the contents of the audio analysis panel
     */
    private void setupWaveformSpectroPanel() {
        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints constraints = new GridBagConstraints();
        waveformSpectroPanel.setLayout(gridbag);
        constraints.fill = GridBagConstraints.BOTH;

        // Add waveform to gridbag
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridwidth = 2;
        constraints.weightx = 1;
        constraints.weighty = 1;
        gridbag.setConstraints(waveformPanel2, constraints);
        waveformSpectroPanel.add(waveformPanel2);
        
        // Add scrubber to gridbag (enforce a minimum height)
        constraints.gridy = 1;
        constraints.weighty = 0;
        constraints.ipady = 5;
        gridbag.setConstraints(scrubberSlider2, constraints);
        waveformSpectroPanel.add(scrubberSlider2);
        
        constraints.gridy = 2;
        constraints.weighty = 1;
        constraints.ipady = 0;
        gridbag.setConstraints(spectrogramPanel, constraints);
        waveformSpectroPanel.add(spectrogramPanel);
    }
    
    /**
     * Method to setup the contents of the audio analysis panel
     */
    private void setupAudioAnalysisPanel() {
        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints constraints = new GridBagConstraints();
        audioAnalysisPanel.setLayout(gridbag);
        constraints.fill = GridBagConstraints.BOTH;
       
        // Add waveform to gridbag
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridwidth = 2;
        constraints.weightx = 1;
        constraints.weighty = 1;
        gridbag.setConstraints(waveformPanel1, constraints);
        audioAnalysisPanel.add(waveformPanel1);
        
        // Add scrubber to gridbag (enforce a minimum height)
        constraints.gridy = 1;
        constraints.weighty = 0;
        constraints.ipady = 5;
        gridbag.setConstraints(scrubberSlider1, constraints);
        audioAnalysisPanel.add(scrubberSlider1);
        
        // Add FFT Panel (Temporary holder)
        constraints.gridy = 2;
        constraints.weighty = 1;
        constraints.gridwidth = 1;
        gridbag.setConstraints(fftPanel, constraints);
        audioAnalysisPanel.add(fftPanel);
    }
    
    /**
     * Method that sets up the waveformPanel and adds a listener to the slider
     */
    private void setupFFTPanel() {

        fftPanel = new FFTPanel(fileHolder, scrubberSlider1);
        

        // Facilitate zooming
        fftPanel.addMouseWheelListener((java.awt.event.MouseWheelEvent evt) -> {
            //fftPanel.mouseWheelMoved(evt);
        });
        // Facilitate start and stop selections 
        fftPanel.addMouseListener(new java.awt.event.MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
                //fftPanel.mousePressed(e);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        fftPanel.addMouseMotionListener(new java.awt.event.MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {
                //fftPanel.mouseDragged(e);
            }

            @Override
            public void mouseMoved(MouseEvent e) {
            }
        });
        
        // Repaint graph everytime the slider is moved
        scrubberSlider1.addChangeListener((javax.swing.event.ChangeEvent evt) -> {
            fftPanel.repaint();
        });
    }
    
    /**
     * Method that sets up the waveformPanel and adds a listener to the slider
     */
    private void setupSpectrogramPanel() {
        spectrogramPanel = new SpectrogramPanel(fileHolder, scrubberSlider2);
        
        // Facilitate zooming
        spectrogramPanel.addMouseWheelListener((java.awt.event.MouseWheelEvent evt) -> {
            spectrogramPanel.mouseWheelMoved(evt);
            waveformPanel2.mouseWheelMoved(evt);
        });
        // Facilitate start and stop selections 
        spectrogramPanel.addMouseListener(new java.awt.event.MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
                spectrogramPanel.mousePressed(e);
                waveformPanel2.mousePressed(e);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        spectrogramPanel.addMouseMotionListener(new java.awt.event.MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {
                spectrogramPanel.mouseDragged(e);
                waveformPanel2.mouseDragged(e);
            }

            @Override
            public void mouseMoved(MouseEvent e) {
            }
        });
        
        // Repaint graph everytime the slider is moved
        scrubberSlider2.addChangeListener((javax.swing.event.ChangeEvent evt) -> {
            spectrogramPanel.repaint();
        });
    }
    
    /**
     * Method that sets up the waveformPanel and adds a listener to the slider
     */
    private void setupWaveformPanels() {
        waveformPanel1 = new WaveformPanel(fileHolder, scrubberSlider1);
        waveformPanel2 = new WaveformPanel(fileHolder, scrubberSlider2);
        
        // Facilitate zooming
        waveformPanel1.addMouseWheelListener((java.awt.event.MouseWheelEvent evt) -> {
            waveformPanel1.mouseWheelMoved(evt);
        });
        waveformPanel2.addMouseWheelListener((java.awt.event.MouseWheelEvent evt) -> {
            waveformPanel2.mouseWheelMoved(evt);
            spectrogramPanel.mouseWheelMoved(evt);
        });
        // Facilitate start and stop selections 
        waveformPanel1.addMouseListener(new java.awt.event.MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
                waveformPanel1.mousePressed(e);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        waveformPanel2.addMouseListener(new java.awt.event.MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
                waveformPanel2.mousePressed(e);
                spectrogramPanel.mousePressed(e);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        waveformPanel1.addMouseMotionListener(new java.awt.event.MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {
                waveformPanel1.mouseDragged(e);
            }

            @Override
            public void mouseMoved(MouseEvent e) {
            }
        });
        waveformPanel2.addMouseMotionListener(new java.awt.event.MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {
                waveformPanel2.mouseDragged(e);
                spectrogramPanel.mouseDragged(e);
            }

            @Override
            public void mouseMoved(MouseEvent e) {
            }
        });
        
        // Repaint graph everytime the slider is moved
        scrubberSlider1.addChangeListener((javax.swing.event.ChangeEvent evt) -> {
            waveformPanel1.repaint();
        });
        scrubberSlider2.addChangeListener((javax.swing.event.ChangeEvent evt) -> {
            waveformPanel2.repaint();
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        buttonsPanel = new javax.swing.JPanel();
        playButton = new javax.swing.JButton();
        recordButton = new javax.swing.JButton();
        stopButton = new javax.swing.JButton();
        nextButton = new javax.swing.JButton();
        backButton = new javax.swing.JButton();
        promptPanel = new javax.swing.JPanel();
        promptLabel = new javax.swing.JLabel();
        errorReportPanel = new javax.swing.JPanel();
        errorPromptLabel = new javax.swing.JLabel();
        errorPromptTextField = new javax.swing.JTextField();
        hypothesisLabel = new javax.swing.JLabel();
        hypothesisTextField = new javax.swing.JTextField();
        statsLabel = new javax.swing.JLabel();
        statsScrollPane = new javax.swing.JScrollPane();
        statsTextArea = new javax.swing.JTextArea();
        graphTabbedPane = new javax.swing.JTabbedPane();
        audioAnalysisPanel = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        advancedErrorList = new javax.swing.JList<>();
        waveformSpectroPanel = new javax.swing.JPanel();
        mainMenuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        loadMenuItem = new javax.swing.JMenuItem();
        exitMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        mainPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Speech Therapy Application"));

        buttonsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Controls"));

        playButton.setText("Play");
        playButton.setToolTipText("Play current recording");
        playButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                playButtonActionPerformed(evt);
            }
        });

        recordButton.setText("Record");
        recordButton.setToolTipText("Start recording audio");
        recordButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                recordButtonActionPerformed(evt);
            }
        });

        stopButton.setText("Stop");
        stopButton.setToolTipText("Stop recording audio");
        stopButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stopButtonActionPerformed(evt);
            }
        });

        nextButton.setText("Next");
        nextButton.setToolTipText("Go to next prompt");
        nextButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nextButtonActionPerformed(evt);
            }
        });

        backButton.setText("Back");
        backButton.setToolTipText("Go to previous prompt");
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout buttonsPanelLayout = new javax.swing.GroupLayout(buttonsPanel);
        buttonsPanel.setLayout(buttonsPanelLayout);
        buttonsPanelLayout.setHorizontalGroup(
            buttonsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(buttonsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(playButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(recordButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(stopButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nextButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(backButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        buttonsPanelLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {backButton, nextButton, playButton, recordButton, stopButton});

        buttonsPanelLayout.setVerticalGroup(
            buttonsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(buttonsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(buttonsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(playButton)
                    .addComponent(recordButton)
                    .addComponent(stopButton)
                    .addComponent(nextButton)
                    .addComponent(backButton))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        buttonsPanelLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {backButton, nextButton, playButton, recordButton, stopButton});

        promptPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Prompt"));

        promptLabel.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        promptLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        promptLabel.setText("Click \"Next\" to get a new prompt...");

        javax.swing.GroupLayout promptPanelLayout = new javax.swing.GroupLayout(promptPanel);
        promptPanel.setLayout(promptPanelLayout);
        promptPanelLayout.setHorizontalGroup(
            promptPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(promptPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(promptLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        promptPanelLayout.setVerticalGroup(
            promptPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(promptLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        errorReportPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Error Report"));

        errorPromptLabel.setFont(new java.awt.Font("Tahoma", 2, 11)); // NOI18N
        errorPromptLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        errorPromptLabel.setText("Prompt");

        hypothesisLabel.setFont(new java.awt.Font("Tahoma", 2, 11)); // NOI18N
        hypothesisLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        hypothesisLabel.setText("Hypothesis");

        statsLabel.setFont(new java.awt.Font("Tahoma", 2, 11)); // NOI18N
        statsLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        statsLabel.setText("Stats");

        statsTextArea.setColumns(20);
        statsTextArea.setRows(5);
        statsScrollPane.setViewportView(statsTextArea);

        javax.swing.GroupLayout errorReportPanelLayout = new javax.swing.GroupLayout(errorReportPanel);
        errorReportPanel.setLayout(errorReportPanelLayout);
        errorReportPanelLayout.setHorizontalGroup(
            errorReportPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, errorReportPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(errorReportPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(statsLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(errorPromptLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(errorPromptTextField)
                    .addComponent(hypothesisLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(statsScrollPane, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
                    .addComponent(hypothesisTextField, javax.swing.GroupLayout.Alignment.LEADING))
                .addContainerGap())
        );
        errorReportPanelLayout.setVerticalGroup(
            errorReportPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(errorReportPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(errorPromptLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(errorPromptTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(hypothesisLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(hypothesisTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(statsLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(statsScrollPane)
                .addContainerGap())
        );

        graphTabbedPane.setName("graphTabbedPane"); // NOI18N

        audioAnalysisPanel.setName("audioAnalysisPanel"); // NOI18N

        javax.swing.GroupLayout audioAnalysisPanelLayout = new javax.swing.GroupLayout(audioAnalysisPanel);
        audioAnalysisPanel.setLayout(audioAnalysisPanelLayout);
        audioAnalysisPanelLayout.setHorizontalGroup(
            audioAnalysisPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGap(0, 666, Short.MAX_VALUE)
        );
        audioAnalysisPanelLayout.setVerticalGroup(
            audioAnalysisPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 445, Short.MAX_VALUE)
        );

        graphTabbedPane.addTab("Audio Analysis", audioAnalysisPanel);

        waveformSpectroPanel.setName("waveformSpectroPanel"); // NOI18N

        advancedErrorList.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane2.setViewportView(advancedErrorList);

        graphTabbedPane.addTab("Advanced Error Reporting", jScrollPane2);
        javax.swing.GroupLayout waveformSpectroPanelLayout = new javax.swing.GroupLayout(waveformSpectroPanel);
        waveformSpectroPanel.setLayout(waveformSpectroPanelLayout);
        waveformSpectroPanelLayout.setHorizontalGroup(
            waveformSpectroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 666, Short.MAX_VALUE)
        );
        waveformSpectroPanelLayout.setVerticalGroup(
            waveformSpectroPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 445, Short.MAX_VALUE)
        );

        graphTabbedPane.addTab("Waveform and Spectrogram", waveformSpectroPanel);

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addComponent(graphTabbedPane)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(errorReportPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addComponent(buttonsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(promptPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(promptPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(buttonsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(errorReportPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(graphTabbedPane))
                .addContainerGap())
        );

        fileMenu.setText("File");

        loadMenuItem.setText("Load");
        loadMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(loadMenuItem);

        exitMenuItem.setText("Exit");
        exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exitMenuItem);

        mainMenuBar.add(fileMenu);

        setJMenuBar(mainMenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Exits application from menu
     * @param evt 
     */
    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
        System.exit(0);
    }//GEN-LAST:event_exitMenuItemActionPerformed

    /**
     * Starts recording from button press
     * @param evt 
     */
    private void recordButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_recordButtonActionPerformed
        recorder.startRecording(promptLabel.getText());
    }//GEN-LAST:event_recordButtonActionPerformed

    /**
     * Stops recording from button press. Updates hypothesis and sets
     * waveformPanel audio file.
     * @param evt 
     */
    private void stopButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stopButtonActionPerformed
        recorder.stopRecording();
        String result = decoder.getPhonesHypothesis();
        waveformPanel1.update();
        waveformPanel2.update();
        fftPanel.update();
        spectrogramPanel.update();
        hypothesisTextField.setText(result);
        float accuracy = decoder.getAccuracy(errorPromptTextField.getText(), result);
        String incorrect = decoder.getMissedPhones();
        statsTextArea.setText("Accuracy: " + String.valueOf(accuracy*100) + "%\n" + "You missed these phones: " + incorrect);

        //populate advanced error reporting tab
        advancedErrorList.setModel(AdvancedErrorReporting.listcontents());
    }//GEN-LAST:event_stopButtonActionPerformed

    /**
     * Changes the prompt to the next prompt. Updates the actual phones.
     * @param evt 
     */
    private void nextButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nextButtonActionPerformed
        String newPrompt = promptLibrary.getNextPrompt();
        promptLabel.setText(newPrompt);
        errorPromptTextField.setText(decoder.getPhonesActual(newPrompt));
    }//GEN-LAST:event_nextButtonActionPerformed

    /**
     * Changes the prompt to the previous prompt. Updates the actual phones.
     * @param evt 
     */
    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
        String newPrompt = promptLibrary.getPreviousPrompt();
        promptLabel.setText(newPrompt);
        errorPromptTextField.setText(decoder.getPhonesActual(newPrompt));
    }//GEN-LAST:event_backButtonActionPerformed

    /**
     * Plays the recording from the current position on the waveform panel.
     * @param evt 
     */
    private void playButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_playButtonActionPerformed
        ArrayList<JSlider> sliders = new ArrayList<>();
        sliders.add(scrubberSlider1);
        sliders.add(scrubberSlider2);
        Component selectedComponent = graphTabbedPane.getSelectedComponent();
        int startFrame, endFrame;
        switch (selectedComponent.getName()) {
            case "audioAnalysisPanel": 
                startFrame = waveformPanel1.getStartFrame();
                endFrame = waveformPanel1.getEndFrame();
                break;
            case "waveformSpectroPanel": 
                startFrame = waveformPanel2.getStartFrame();
                endFrame = waveformPanel2.getEndFrame();
                break;
            default:
                startFrame = 0;
                endFrame = scrubberSlider1.getMaximum();
        }
        recorder.playRecording(sliders, startFrame, endFrame);
    }//GEN-LAST:event_playButtonActionPerformed

    /**
     * Attempts to load an audio file from the computer. Updates hypothesis and
     * waveformPanel.
     * @param evt 
     */
    private void loadMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadMenuItemActionPerformed
        // Allow user to load file
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
            "WAV files", "wav");
        chooser.setFileFilter(filter);
        int returnVal = chooser.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            // TODO: fill in prompt if possible
            // Fill in hypothesis
            fileHolder.update(chooser.getSelectedFile());
            recorder.setWaveFile(chooser.getSelectedFile());
            String result = decoder.getPhonesHypothesis();
            hypothesisTextField.setText(result);
        
            //populate stats field
            float accuracy = decoder.getAccuracy(errorPromptTextField.getText(), result);
            String incorrect = decoder.getMissedPhones();
            statsTextArea.setText("Accuracy: " + String.valueOf(accuracy*100) + "%\n" + "You missed these phones: " + incorrect);

            waveformPanel1.update();
            waveformPanel2.update();
            spectrogramPanel.update();
            fftPanel.update();
        }
    }//GEN-LAST:event_loadMenuItemActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ApplicationUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ApplicationUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ApplicationUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ApplicationUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        fileHolder = new FileHolder();
        recorder = new SoundRecorder(fileHolder);
        promptLibrary = new PromptLibrary("prompts.txt");
        decoder = new Decoder(fileHolder);
        scrubberSlider1 = new JSlider(0, 0);
        scrubberSlider2 = new JSlider(0, 0);
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new ApplicationUI().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JList<String> advancedErrorList;
    private javax.swing.JPanel audioAnalysisPanel;
    private javax.swing.JButton backButton;
    private javax.swing.JPanel buttonsPanel;
    private javax.swing.JLabel errorPromptLabel;
    private javax.swing.JTextField errorPromptTextField;
    private javax.swing.JPanel errorReportPanel;
    private javax.swing.JMenuItem exitMenuItem;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JTabbedPane graphTabbedPane;
    private javax.swing.JLabel hypothesisLabel;
    private javax.swing.JTextField hypothesisTextField;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JMenuItem loadMenuItem;
    private javax.swing.JMenuBar mainMenuBar;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JButton nextButton;
    private javax.swing.JButton playButton;
    private javax.swing.JLabel promptLabel;
    private javax.swing.JPanel promptPanel;
    private javax.swing.JButton recordButton;
    private javax.swing.JLabel statsLabel;
    private javax.swing.JScrollPane statsScrollPane;
    private javax.swing.JTextArea statsTextArea;
    private javax.swing.JButton stopButton;
    private javax.swing.JPanel waveformSpectroPanel;
    // End of variables declaration//GEN-END:variables
}
